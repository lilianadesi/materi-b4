import { reactive } from "vue";

const state = reactive({
  user: null,
});

const methods = {
  setUSer(payload) {
    state.user = payload ? payload.user : null;
  },
};

export default {
  state,
  methods,
};
